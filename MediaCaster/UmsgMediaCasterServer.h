/*
 * UmsgMediaCasterServer.h
 *
 *  Created on: 2016年7月15日
 *      Author: terry
 */

#ifndef UMSGMEDIACASTERSERVER_H_
#define UMSGMEDIACASTERSERVER_H_

#include "BasicMediaCasterServer.h"
#include "UmsgMediaCasterConnection.h"
#include <udt.h>
#include "TThread.h"
#include "TMap.h"


namespace av
{

typedef std::shared_ptr< UmsgMediaCasterConnection >		UmsgMediaCasterConnectionPtr;


class UmsgMediaCasterServer: public BasicMediaCasterServer, public comn::Thread
{
public:
	static void Startup();
	static void Cleanup();

public:
	UmsgMediaCasterServer();
	virtual ~UmsgMediaCasterServer();

	virtual bool start(const char* ip, int port, const std::string& params);
	virtual bool isStarted();
	virtual void stop();

	virtual void close(const char* name);
	virtual void closeAll();


	void onAcquireStream(const std::string& name, UmsgMediaCasterConnectionPtr& conn);

	void remove(UmsgMediaCasterConnectionPtr& conn);

protected:
    virtual void onClose(UmsgMediaCasterConnectionPtr& conn);

protected:
	virtual int run();
	virtual void doStop();

protected:
	int	m_port;
	UDTSOCKET m_socket;
	int     m_epoll;

	typedef comn::Map< UDTSOCKET, UmsgMediaCasterConnectionPtr >	ConnectionMap;
	ConnectionMap	m_connMap;

};


} /* namespace av */

#endif /* UMSGMEDIACASTERSERVER_H_ */
