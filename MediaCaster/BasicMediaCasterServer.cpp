/*
 * BasicMediaCasterServer.cpp
 *
 *  Created on: 2016年7月14日
 *      Author: terry
 */

#include "BasicMediaCasterServer.h"

namespace av
{


BasicMediaCasterServer::BasicMediaCasterServer():
		m_sink()
{
}

BasicMediaCasterServer::~BasicMediaCasterServer()
{
}

void BasicMediaCasterServer::setEventSink(MediaCasterEventSink* sink)
{
	comn::AutoCritSec lock(m_cs);
	m_sink = sink;
}

void BasicMediaCasterServer::fireEvent(MediaCasterEvent& event, int protocol)
{
	if (m_sink)
	{
		m_sink->onCasterEvent(event, protocol);
	}
}

} /* namespace av */
