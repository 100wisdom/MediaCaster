/*
 * CMediaCasterManager.h
 *
 *  Created on: 2016-6-21
 *      Author: terry
 */

#ifndef CMEDIACASTERMANAGER_H_
#define CMEDIACASTERMANAGER_H_

#include "MediaCaster.h"
#include "TCriticalSection.h"


namespace av
{

typedef std::shared_ptr< MediaCasterServer >	MediaCasterServerPtr;


class CMediaCasterManager: public MediaCasterManager, public MediaCasterEventSink
{
public:
	static CMediaCasterManager& instance();

	static void Startup();
	static void Cleanup();

public:
	CMediaCasterManager();
	virtual ~CMediaCasterManager();

	virtual bool start(int protocol, const char* ip, int port, const std::string& params);

	virtual bool isStarted(int protocol);

	virtual void stop(int protocol);

	virtual void stopAll();

	virtual MediaCasterPtr open(const char* name, const MediaFormat& fmt);
	virtual void close(const char* name);
	virtual MediaCasterPtr find(const char* name);
	virtual void closeAll();

	virtual void setEventSink(MediaCasterEventSink* sink);

protected:
    virtual void onCasterEvent(MediaCasterEvent& event, int protocol);

protected:
	void fireEvent();

	MediaCasterServerPtr getServer(int protocol);

protected:
	MediaCasterEventSink*	m_sink;
	comn::CriticalSection	m_cs;

	MediaCasterServerPtr	m_tcpServer;
	MediaCasterServerPtr	m_udtServer;
	MediaCasterServerPtr	m_tcp2Server;
	MediaCasterServerPtr	m_msgServer;

};



} /* namespace av */

#endif /* CMEDIACASTERMANAGER_H_ */
