/*
 * BasicMediaCasterServer.h
 *
 *  Created on: 2016年7月14日
 *      Author: terry
 */

#ifndef BASICMEDIACASTERSERVER_H_
#define BASICMEDIACASTERSERVER_H_

#include "MediaCaster.h"
#include "TCriticalSection.h"


namespace av
{

class BasicMediaCasterServer: public MediaCasterServer
{
public:
	BasicMediaCasterServer();
	virtual ~BasicMediaCasterServer();

	virtual void setEventSink(MediaCasterEventSink* sink);

protected:
	void fireEvent(MediaCasterEvent& event, int protocol);

protected:
	comn::CriticalSection m_cs;
	MediaCasterEventSink*	m_sink;

};

} /* namespace av */

#endif /* BASICMEDIACASTERSERVER_H_ */
