/*
 * TcpMediaCasterConnection.h
 *
 *  Created on: 2016��6��21��
 *      Author: terry
 */

#ifndef TCPMEDIACASTERCONNECTION_H_
#define TCPMEDIACASTERCONNECTION_H_

#include "TcpConnection.h"
#include "MediaStream.h"
#include "MediaTransport.h"
#include "TByteBuffer.h"
#include <json/json.h>
#include "MediaPacketQueue.h"


namespace av
{


class TcpMediaCasterConnection: public TcpConnection ,
                                public av::MediaSink,
                                public std::enable_shared_from_this< TcpMediaCasterConnection >
{
public:
	TcpMediaCasterConnection();
	virtual ~TcpMediaCasterConnection();

    virtual int onOpen();

    virtual void onClose();

    virtual void onWrite(struct bufferevent *bev);

    virtual void onRead(struct bufferevent *bev);

    virtual void onEvent(struct bufferevent *bev, short events);


    virtual void onMediaFormat(const av::MediaFormat& fmt);

    virtual void onMediaPacket(av::MediaPacketPtr& pkt);

    virtual void onMediaEvent(int event);

    std::string getStreamName();

protected:
    bool isStreaming();

    bool handleCommand(const MediaTransportHeader& header, Json::Value& req, Json::Value& resp);
    bool handlePlay(const MediaTransportHeader& header, Json::Value& req, Json::Value& resp);

    int writeMedia(av::MediaPacketPtr& pkt);

    void getConfigSize();

protected:
    std::string m_peerID;    
    std::string m_streamName;
    int m_streamState;

    MediaTransportHeader	m_header;
    bool	m_headerReady;
    comn::ByteBuffer m_jsonBuffer;
    
    av::MediaFormat m_format;

    bool    m_jamming;  /// �Ƿ�ӵ��
    size_t	m_maxCacheSize;

    MediaPacketQueue    m_pktQueue;
    bool    m_empty;

};



} /* namespace av */

#endif /* TCPMEDIACASTERCONNECTION_H_ */
