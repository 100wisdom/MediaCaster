/*
 * TcpMediaCasterServer.h
 *
 *  Created on: 2016-6-20
 *      Author: terry
 */

#ifndef TCPMEDIACASTERSERVER_H_
#define TCPMEDIACASTERSERVER_H_

#include "BasicMediaCasterServer.h"
#include "TMap.h"
#include "TcpServer.h"


namespace av
{


class TcpMediaCasterServer: public BasicMediaCasterServer, public TcpServer
{
public:
	TcpMediaCasterServer();
	virtual ~TcpMediaCasterServer();

	virtual bool start(const char* ip, int port, const std::string& params);
	virtual bool isStarted();
	virtual void stop();

	virtual void close(const char* name);
	virtual void closeAll();

    void onAcquireStream(const std::string& name, TcpConnectionPtr& conn);

protected:
	virtual TcpConnectionPtr create(evutil_socket_t fd);
    virtual void onClose(TcpConnectionPtr& conn);

protected:
	typedef comn::Map<std::string, MediaCasterPtr>	MediaCasterMap;


};



} /* namespace av */

#endif /* TCPMEDIACASTERSERVER_H_ */
