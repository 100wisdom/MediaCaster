/*
 * MediaCasterTable.h
 *
 *  Created on: 2016��6��21��
 *      Author: terry
 */

#ifndef MEDIACASTERTABLE_H_
#define MEDIACASTERTABLE_H_

#include "CMediaCaster.h"
#include "TMap.h"
#include "TSingleton.h"

namespace av
{


class MediaCasterTable : public comn::Map< std::string, CMediaCasterPtr >
{
public:
	static MediaCasterTable& instance();

public:
	MediaCasterTable();
	virtual ~MediaCasterTable();
};



} /* namespace av */

#endif /* MEDIACASTERTABLE_H_ */
