/*
 * MediaCasterTable.cpp
 *
 *  Created on: 2016��6��21��
 *      Author: terry
 */

#include "MediaCasterTable.h"

namespace av
{

typedef comn::Singleton< MediaCasterTable >		MediaCasterTableSingleton;


MediaCasterTable& MediaCasterTable::instance()
{
	return MediaCasterTableSingleton::instance();
}


MediaCasterTable::MediaCasterTable()
{
}

MediaCasterTable::~MediaCasterTable()
{
}

} /* namespace av */
