/*
 * MediaCasterConfig.h
 *
 *  Created on: 2016年7月14日
 *      Author: terry
 */

#ifndef MEDIACASTERCONFIG_H_
#define MEDIACASTERCONFIG_H_

namespace av
{

class MediaCasterConfig
{
public:
	MediaCasterConfig();
	virtual ~MediaCasterConfig();

	static int s_queueDuration;
	static int s_sendBufSize;

};


} /* namespace av */

#endif /* MEDIACASTERCONFIG_H_ */
