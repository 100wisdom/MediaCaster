/*
 * MediaCasterConfig.cpp
 *
 *  Created on: 2016年7月14日
 *      Author: terry
 */

#include "MediaCasterConfig.h"

namespace av
{

int MediaCasterConfig::s_queueDuration = 1000 * 1000;
int MediaCasterConfig::s_sendBufSize = 0;


MediaCasterConfig::MediaCasterConfig()
{
}

MediaCasterConfig::~MediaCasterConfig()
{
}


} /* namespace av */
