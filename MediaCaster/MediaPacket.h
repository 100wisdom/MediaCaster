﻿/*    file: MediaPacket.h
 *    desc:
 *   
 * created: 2014-07-08 18:58:08
 *  author: zhengchuanjiang
 * company: 
 */ 


#if !defined MEDIAPACKET_H_
#define MEDIAPACKET_H_

#include "MemBuffer.h"
#include "SharedPtr.h"
////////////////////////////////////////////////////////////////////////////
namespace av
{

enum MediaFlag
{
    MEDIA_FLAG_KEY = 0x01
};


/**
 * 媒体包
 */
class MediaPacket
{
public:
    MediaPacket();
    ~MediaPacket();

    explicit MediaPacket(size_t length);

    size_t	capacity() const;

    uint8_t*    data;	/// 数据指针, 该指针由 m_buffer 管理
    int         size;

    int     type;
    int64_t	pts;
    int32_t	duration;
    int		flags;

    /**
     * 确保有指定长度的容量
     * @param length
     */
    void ensure(size_t length);

    void clear();

    void resize(size_t length);

    /**
     * 往缓冲区继续写入多个字节
     * @param p
     * @param length
     */
    void write(const uint8_t* p, size_t length);

    /**
     * 把缓冲区设为指定的多个字节
     * @param p
     * @param length
     */
    void set(const uint8_t* p, size_t length);

    bool empty() const;

    bool hasFlags(int flags) const;

    /**
     * 克隆媒体包
     * @param pkt
     */
    void clone(const MediaPacket& pkt);

    /**
     * 拷贝媒体包属性, 不包括data和size
     * @param pkt
     */
    void copyProp(const MediaPacket& pkt);

    bool isVideo() const;

    bool isKey() const;

protected:

protected:
    MemBuffer	m_buffer;

};

typedef std::shared_ptr< MediaPacket >		MediaPacketPtr;

}

////////////////////////////////////////////////////////////////////////////
#endif //MEDIAPACKET_H_

