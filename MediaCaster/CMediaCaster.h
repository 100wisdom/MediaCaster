/*
 * CMediaCaster.h
 *
 *  Created on: 2016��6��21��
 *      Author: terry
 */

#ifndef CMEDIACASTER_H_
#define CMEDIACASTER_H_

#include "MediaCaster.h"
#include "MediaStream.h"
#include "MediaSourceT.h"

namespace av
{

class MediaCasterSource : public MediaCaster, public MediaSource
{
public:

};


class CMediaCaster: public MediaSourceT< MediaCasterSource >
{
public:
	CMediaCaster();
	virtual ~CMediaCaster();

	virtual int open(const std::string& url, const std::string& params);
	virtual void close();
	virtual bool isOpen();

    virtual const char* getName();
	virtual bool getFormat(MediaFormat& fmt);
	virtual bool setFormat(const MediaFormat& fmt);

	virtual bool write(int type, uint8_t* data, int size, int64_t pts, int duration, int flags);

protected:
    void parseParamSet(MediaPacket& pkt);
    bool parseH264ParamSet(MediaPacket& pkt);
    bool parseH265ParamSet(MediaPacket& pkt);

    void parseH264Nalu(const uint8_t* data, size_t length);
    void parseH265Nalu(const uint8_t* data, size_t length);

    void correctDuration(MediaPacket& pkt);

protected:
    std::string m_vps;
    std::string m_sps;
    std::string m_pps;

    int64_t m_videoLastPts;
    int64_t m_audioLastPts;
    int	m_videoClock;
    int m_audioClock;

};


typedef std::shared_ptr< CMediaCaster >		CMediaCasterPtr;



} /* namespace av */

#endif /* CMEDIACASTER_H_ */
