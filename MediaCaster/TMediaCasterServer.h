/*
 * TMediaCasterServer.h
 *
 *  Created on: 2016年7月14日
 *      Author: terry
 */

#ifndef TMEDIACASTERSERVER_H_
#define TMEDIACASTERSERVER_H_

#include "BasicMediaCasterServer.h"
#include "TMediaCasterConnection.h"
#include "TMap.h"
#include "SocketPair.h"


namespace av
{

typedef std::shared_ptr< TMediaCasterConnection >		TMediaCasterConnectionPtr;

class TMediaCasterServer: public BasicMediaCasterServer, public comn::Thread
{
public:
	TMediaCasterServer();
	virtual ~TMediaCasterServer();

	virtual bool start(const char* ip, int port, const std::string& params);
	virtual bool isStarted();
	virtual void stop();

	virtual void close(const char* name);
	virtual void closeAll();


	void onAcquireStream(const std::string& name, TMediaCasterConnectionPtr& conn);

	void remove(TMediaCasterConnectionPtr& conn);

protected:
	virtual void onClose(TMediaCasterConnectionPtr& conn);


	virtual int run();
	virtual void doStop();


protected:
	comn::Socket	m_socket;
	int		m_port;

	typedef comn::Map< socket_t, TMediaCasterConnectionPtr >	ConnectionMap;
	ConnectionMap	m_connMap;


};



} /* namespace av */

#endif /* TMEDIACASTERSERVER_H_ */
