﻿/*
 * MemBuffer.h
 *
 *  Created on: 2014年6月7日
 *      Author: terry
 */

#ifndef MEMBUFFER_H_
#define MEMBUFFER_H_

#include "BasicType.h"

class  MemBuffer
{
public:
	MemBuffer();
	virtual ~MemBuffer();

	MemBuffer(size_t capacity);

	MemBuffer(const MemBuffer& other);

	MemBuffer& operator = (const MemBuffer& other);

	void assign(const MemBuffer& other);

	uint8_t* data();
	size_t	size() const;
	size_t	capacity() const;

	bool empty() const;

	void resize(size_t length);

	bool ensure(size_t capacity);

	void clear();

	bool write(const uint8_t* data, size_t length);
	bool write(const char* data, size_t length);


protected:
	void cleanup();

protected:
	size_t	m_capacity;
	size_t	m_length;
	uint8_t*	m_data;
};

#endif /* MEMBUFFER_H_ */
