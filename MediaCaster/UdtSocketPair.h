/*
 * UdtSocketPair.h
 *
 *  Created on: 2016年6月24日
 *      Author: terry
 */

#ifndef UDTSOCKETPAIR_H_
#define UDTSOCKETPAIR_H_

#include <udt.h>


namespace util
{

class UdtSocketPair
{
public:
	UdtSocketPair();
	virtual ~UdtSocketPair();

	bool open();
	void close();

	void makeReadable();
	void clearReadable();

	UDTSOCKET getHandle() const;
	UDTSOCKET getPeerHandle() const;

protected:
    bool makeConnection(UDTSOCKET& listenSocket, sockaddr_in& addr);

private:
    UDTSOCKET	m_listen;
    UDTSOCKET   m_serverSocket;
    UDTSOCKET   m_clientSocket;

};


} /* namespace util */

#endif /* UDTSOCKETPAIR_H_ */
