/*
 * MediaPacketQueue.h
 *
 *  Created on: 2016年4月6日
 *      Author: terry
 */

#ifndef MEDIAPACKETQUEUE_H_
#define MEDIAPACKETQUEUE_H_

#include "MediaPacket.h"
#include <deque>
#include "TCriticalSection.h"
#include "TEvent.h"


namespace av
{

class MediaPacketQueue
{
public:
	MediaPacketQueue();
	virtual ~MediaPacketQueue();

	size_t size();

	bool empty();

	size_t push(MediaPacketPtr& packet);

	MediaPacketPtr pop();

	bool pop(MediaPacketPtr& packet);

	void clear();

	/**
	 * 在指定时间内等待媒体包
	 * 如果队列不空,立即返回true
	 * @param ms	毫秒
	 * @return true 表示队列有元素了
	 */
	bool timedwait(int ms);

	/**
	 * 如果队列为空, 等待, 并弹出头部元素
	 * @param packet
	 * @param ms
	 * @return
	 */
	bool pop(MediaPacketPtr& packet, int ms);

	/// 取消等待
	void cancelWait();

	/**
	 * 获取媒体时长, 单位为微妙
	 * @return
	 */
	int64_t getDuration();

	/**
	 * 删除队列前面的包, 直到视频关键帧
	 * 如果队列中没有视频关键帧, 不做任何操作
	 * @return 删除的包数量
	 */
	size_t dropUntilKeyFrame();

protected:
    typedef std::deque< MediaPacketPtr >  PacketDeque;

    size_t findVideoKey();

    size_t rfindVideoKey();

protected:
    PacketDeque m_packets;
    comn::CriticalSection   m_cs;
    comn::Event	m_event;


};



} /* namespace av */

#endif /* MEDIAPACKETQUEUE_H_ */
