/*
 * UdtMediaCasterServer.h
 *
 *  Created on: 2016年6月23日
 *      Author: terry
 */

#ifndef UDTMEDIACASTERSERVER_H_
#define UDTMEDIACASTERSERVER_H_

#include "BasicMediaCasterServer.h"
#include "UdtMediaCasterConnection.h"
#include <udt.h>
#include "TThread.h"
#include "TMap.h"


namespace av
{

typedef std::shared_ptr< UdtMediaCasterConnection >		UdtMediaCasterConnectionPtr;


class UdtMediaCasterServer: public BasicMediaCasterServer, public comn::Thread
{
public:
	static void Startup();
	static void Cleanup();

public:
	UdtMediaCasterServer();
	virtual ~UdtMediaCasterServer();

	virtual bool start(const char* ip, int port, const std::string& params);
	virtual bool isStarted();
	virtual void stop();

	virtual void close(const char* name);
	virtual void closeAll();


	void onAcquireStream(const std::string& name, UdtMediaCasterConnectionPtr& conn);

	void remove(UdtMediaCasterConnectionPtr& conn);

protected:
    virtual void onClose(UdtMediaCasterConnectionPtr& conn);

protected:
	virtual int run();
	virtual void doStop();

protected:
	int	m_port;
	UDTSOCKET m_socket;
	int     m_epoll;

	typedef comn::Map< UDTSOCKET, UdtMediaCasterConnectionPtr >	ConnectionMap;
	ConnectionMap	m_connMap;

};


} /* namespace av */

#endif /* UDTMEDIACASTERSERVER_H_ */
