/*
 * MediaCaster.h
 *
 *  Created on: 2016-6-20
 *      Author: terry
 */

#ifndef MEDIACASTER_H_
#define MEDIACASTER_H_


#include "MediaFormat.h"
#include "SharedPtr.h"
#include <string>


namespace av
{



class MediaCaster
{
public:
	virtual ~MediaCaster() {}

    virtual const char* getName() =0;

	virtual bool getFormat(MediaFormat& fmt) =0;
	virtual bool setFormat(const MediaFormat& fmt) =0;

	virtual bool write(int type, uint8_t* data, int size, int64_t pts, int duration, int flags) =0;

};

typedef std::shared_ptr< MediaCaster >	MediaCasterPtr;


enum MediaCasterEventType
{
	kCaster_acquire = 1,
	kCaster_create,
	kCaster_destroy
};


struct MediaCasterEvent
{
	int type;
	std::string streamName;
    std::string ip;
    int     port;
};


class MediaCasterEventSink
{
public:
	virtual ~MediaCasterEventSink() {}

	virtual void onCasterEvent(MediaCasterEvent& event, int protocol) =0;
};


enum MediaCasterProtocol
{
	kTcpCaster,
	kUdtCaster,
	kMtpCaster,
	kUmsgCaster,
	kRtspCaster
};


class MediaCasterServer
{
public:
	virtual ~MediaCasterServer() {}

	virtual bool start(const char* ip, int port, const std::string& params) =0;
	virtual bool isStarted() =0;
	virtual void stop() =0;

	virtual void close(const char* name) =0;
	virtual void closeAll() =0;

	virtual void setEventSink(MediaCasterEventSink* sink) =0;

};

class MediaCasterManager
{
public:
	virtual ~MediaCasterManager() {}

	virtual bool start(int protocol, const char* ip, int port, const std::string& params) =0;

	virtual bool isStarted(int protocol) =0;

	virtual void stop(int protocol) =0;

	virtual void stopAll() =0;

	virtual MediaCasterPtr open(const char* name, const MediaFormat& fmt) =0;
	virtual void close(const char* name) =0;
	virtual MediaCasterPtr find(const char* name) =0;
	virtual void closeAll() =0;

	virtual void setEventSink(MediaCasterEventSink* sink) =0;

};

DLLEXPORT MediaCasterManager& getCasterManager();




} /* namespace av */


#endif /* MEDIACASTER_H_ */
