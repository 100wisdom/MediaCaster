@rem created: 2016-04-19
@rem  author: chuanjiang.zh@qq.com
@rem   

set DIR_BUILD=%~dp0
set DIR_PROJECT=%DIR_BUILD%..

mkdir %DIR_PROJECT%\vs
cd /D %DIR_PROJECT%\vs

del %DIR_PROJECT%\third_party\Libevent\lib\*.lib
del %DIR_PROJECT%\comn\lib\*.lib
del %DIR_PROJECT%\lib\*.lib
del %DIR_PROJECT%\bin\MediaCaster.dll

@rem cmake ..  -T v140_xp
cmake ..  -G "Visual Studio 14 2015"
cmake --build . --config Release

cd /D %DIR_BUILD%


