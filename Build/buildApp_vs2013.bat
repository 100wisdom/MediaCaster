@rem created: 2016-04-19
@rem  author: chuanjiang.zh@qq.com
@rem   

set DIR_BUILD=%~dp0
set DIR_PROJECT=%DIR_BUILD%..

set DIR_VS=%DIR_PROJECT%\vs2013
mkdir %DIR_VS%
cd /D %DIR_VS%

del %DIR_PROJECT%\third_party\Libevent\lib\*.lib
del %DIR_PROJECT%\comn\lib\*.lib
del %DIR_PROJECT%\lib\*.lib
del %DIR_PROJECT%\bin\MediaCaster.dll

cmake ..  -G "Visual Studio 12 2013" -T v120_xp
cmake --build . --config Release

cd /D %DIR_BUILD%


