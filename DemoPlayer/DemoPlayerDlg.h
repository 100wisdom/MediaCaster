
// DemoPlayerDlg.h : 头文件
//

#pragma once
#include "afxwin.h"

#include "TinyPlayer.h"
#include "SharedPtr.h"
#include "TStringUtil.h"
#include "TStringCast.h"
#include "Process.h"
#include "afxcmn.h"

struct PlaySource
{
    std::string url;

    PlaySource()
    {
    }
};

typedef std::vector< PlaySource >  PlaySourceArray;


// CDemoPlayerDlg 对话框
class CDemoPlayerDlg : public CDialog
{
// 构造
public:
	CDemoPlayerDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_DEMOPLAYER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

protected:
    void init();
    void uninit();

    void play(CString url);
    void stop();


    typedef std::shared_ptr< TinyPlayer >   TinyPlayerPtr;

    TinyPlayerPtr m_player;
    Process m_ffplay;

    CString m_rtspUrl;
    CString m_psUrl;

    PlaySourceArray    m_sources;
    size_t  m_sourceIdx;

protected:
    CString getPlayerCmd();
    
    bool loadSourceFromFile();
    void startSource(const PlaySource& source);

    void testJson();

public:
    afx_msg void OnBnClickedButtonPlay();
    afx_msg void OnBnClickedButtonStop();
    afx_msg void OnDestroy();

    afx_msg void OnBnClickedButtonTest();
    afx_msg void OnBnClickedButtonAddMuch();
    afx_msg void OnBnClickedButtonAdd2();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    CSliderCtrl m_slider;
    afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
};
