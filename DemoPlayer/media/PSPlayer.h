/*
 * PSPlayer.h
 *
 *  Created on: 2016年1月25日
 *      Author: terry
 */

#ifndef PSPLAYER_H_
#define PSPLAYER_H_

#include "TinyPlayer.h"
#include "CMediaPlayer.h"
#include "TThread.h"
#include "TEvent.h"
#include "TCriticalSection.h"
#include "ProgramStreamParser.h"


class TcpJsonAVFormat;

class PSPlayer : public TinyPlayer , public comn::Thread
{
public:
	PSPlayer();
	virtual ~PSPlayer();

    virtual int setParam(const char* key, const char* value);

    virtual int open(const char* url);

    virtual void close();

    virtual bool isOpen();

    virtual void setVideoWnd(HWND hwnd);

protected:
    virtual int run();
    virtual void doStop();

    void onRead(uint8_t* buf, int length);
    void onStreamEnd();

protected:
    CMediaPlayer m_player;
    TcpJsonAVFormat*  m_tcpPs;
    comn::Event m_event;
    comn::CriticalSection m_cs;
    AVDictionary*   m_dict;
    av::ProgramStreamParser m_psParser;

};

#endif /* PSPLAYER_H_ */
