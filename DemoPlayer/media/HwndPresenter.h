/*    file: HwndPresenter.h
 *    desc:
 *   
 * created: 2014-01-21 16:52:09
 *  author: zhengchuanjiang
 * company: 
 */ 


#if !defined HWNDPRESENTER_H_
#define HWNDPRESENTER_H_

#include "BasicType.h"
#include "ScaleContext.h"
#include <string>
////////////////////////////////////////////////////////////////////////////
class HwndPresenter
{
public:
    HwndPresenter();
    ~HwndPresenter();

    void reset();

    bool draw(HWND hwnd, AVFrame* pFrame, int ratioX, int ratioY);

    bool draw(HWND hwnd, AVFrame* pFrame);

    bool drawCache(HWND hwnd, int ratioX, int ratioY);

    static RECT getScaleRect(const RECT& rc, int ratioX, int ratioY);
    static RECT getDestRect(int picWidth, int picHeight, 
        int destWidth, int destHeight,
        int ratioX, int ratioY);

    void setOsd(const std::string& text);

private:
    void drawPicture(HWND hwnd, const RECT& rc, AVPicture& pic, AVFrame* frame);
    void drawPicture(HWND hwnd, const RECT& rc, AVPicture& pic);

    void resetPicture(int width, int height);

    bool scalePicture(AVPicture& outPic, int outWidth, int outHeight,
                AVPicture& srcPic, int srcWidth, int srcHeight);

private:
    ScaleContext    m_context;

    AVPicture       m_picture;
    
    int m_maxWidth;
    int m_maxHeight;

    BITMAPINFO  m_bmpInfo;
    std::string m_osd;


};
////////////////////////////////////////////////////////////////////////////
#endif //HWNDPRESENTER_H_

