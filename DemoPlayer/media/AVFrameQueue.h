/*
 * AVFrameQueue.h
 *
 *  Created on: 2015年6月9日
 *      Author: chuanjiang.zh@qq.com
 */

#ifndef AVFRAMEQUEUE_H_
#define AVFRAMEQUEUE_H_

#include "Ffmpeg.h"
#include "TCriticalSection.h"
#include <deque>

class AVFrameQueue
{
public:
    AVFrameQueue()
    {

    }

    ~AVFrameQueue()
    {
        
    }

    size_t push(AVFrame* pFrame)
    {
        comn::AutoCritSec lock(m_cs);
        m_frames.push_back(pFrame);
        return m_frames.size();
    }

    AVFrame* pop()
    {
        comn::AutoCritSec lock(m_cs);
        AVFrame* frame = NULL;
        if (!m_frames.empty())
        {
            frame = m_frames.front();
            m_frames.pop_front();
        }
        return frame;
    }

    size_t size()
    {
        comn::AutoCritSec lock(m_cs);
        return m_frames.size();
    }

    void clear()
    {
        comn::AutoCritSec lock(m_cs);
        while (!m_frames.empty())
        {
            AVFrame* frame = m_frames.front();
            m_frames.pop_front();
            av_frame_free(&frame);
        }
    }

protected:
    typedef std::deque< AVFrame* >  FrameDeque;

    FrameDeque  m_frames;
    comn::CriticalSection   m_cs;

};



#endif /* AVFRAMEQUEUE_H_ */
