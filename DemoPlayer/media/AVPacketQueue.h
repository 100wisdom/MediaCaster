/*
 * AVPacketQueue.h
 *
 *  Created on: 2015年6月23日
 *      Author: chuanjiang.zh@qq.com
 */

#ifndef AVPACKETQUEUE_H_
#define AVPACKETQUEUE_H_

#include "Ffmpeg.h"
#include <deque>
#include <memory>
#include "SharedPtr.h"
#include "TCriticalSection.h"


typedef std::shared_ptr< AVPacket >     AVPacketPtr;

struct AVPacketDeleter
{
    void operator() (AVPacket* pkt)
    {
        av_free_packet(pkt);
        delete pkt;
    }
};


class AVPacketQueue
{
public:
    static const int VIDEO_INDEX = 0;
    static const int AUDIO_INDEX = 1;

public:
    AVPacketQueue();
    virtual ~AVPacketQueue();

    size_t size();

    bool empty();

    size_t push(AVPacketPtr& packet);

    AVPacketPtr pop();

    bool pop(AVPacketPtr& packet);

    void clear();

    int64_t getDuration();

    bool getHeadTime(int64_t& ts);

    int getHeadFlag();

    size_t push(int index, uint8_t* data, int size, int64_t ts);

protected:
    typedef std::deque< AVPacketPtr >  PacketDeque;

protected:
    PacketDeque m_packets;
    comn::CriticalSection   m_cs;

};

#endif /* AVPACKETQUEUE_H_ */
