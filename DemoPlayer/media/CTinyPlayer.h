/*    file: CTinyPlayer.h
 *    desc:
 *   
 * created: 2015-11-22
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined CTINYPLAYER_H_
#define CTINYPLAYER_H_

#include "TinyPlayer.h"
#include "CMediaPlayer.h"
#include "TEvent.h"
#include "TCriticalSection.h"
#include "EofDetector.h"

class TcpJsonAVFormat;

class CTinyPlayer : public TinyPlayer, public comn::Thread
{
public:
    CTinyPlayer();
    ~CTinyPlayer();

    virtual int setParam(const char* key, const char* value);

    virtual int open(const char* url);

    virtual void close();

    virtual bool isOpen();

    virtual void setVideoWnd(HWND hwnd);

    virtual int seek(double position);

protected:
    virtual int run();
    virtual bool startup();
    virtual void cleanup();
    virtual void doStop();

protected:
    void onStreamEnd();
    
protected:
    AVDictionary*   m_dict;
    CMediaPlayer m_player;
    AVFormatContext* m_fmtContext;
    int m_videoIdx;
    int m_audioIdx;

    comn::Event m_event;
    comn::CriticalSection m_cs;

    TcpJsonAVFormat*  m_psFormat;
    EofDetector m_eofDetector;
    double  m_position;

};

#endif //CTINYPLAYER_H_

