/*
 * MediaPlayer.h
 *
 *  Created on: 2015年6月1日
 *      Author: chuanjiang.zh@qq.com
 */

#ifndef MEDIAPLAYER_H_
#define MEDIAPLAYER_H_

#include "MediaType.h"
#include <string.h>


class MediaPlayer
{
public:
    enum State
    {
        kStopped =0,
        kPaused,
        kPlaying,
    };

public:
    virtual ~MediaPlayer() {}

    virtual int open(int vcodec, int acodec, int samplerate, int channels) =0;

    virtual void close() =0;

    virtual bool isOpen() =0;

    virtual void setVideoWnd(HWND hwnd) =0;

    virtual int input(const MediaPacket& pkt) =0;


};






#endif /* MEDIAPLAYER_H_ */
