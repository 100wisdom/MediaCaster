/*    file: RapidJsonBuilder.h
 *    desc:
 *   
 * created: 2015-11-24
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined RAPIDJSONBUILDER_H_
#define RAPIDJSONBUILDER_H_

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"


using namespace rapidjson;

class JValue
{
public:
    JValue(Document& doc):
        m_value(doc),
        m_doc(doc)
    {
    }

    JValue(Value& v, Document& doc):
      m_value(v),
      m_doc(doc)
    {
    }

    template < class T >
    JValue add(const std::string& name, const T& t)
    {
        m_value.AddMember(StringRef(name.c_str()), t, m_doc.GetAllocator());
        return JValue(m_value[name.c_str()], m_doc);
    }

    template <>
    JValue add(const std::string& name, const std::string& value)
    {
        Value key(name.c_str(), name.size(), m_doc.GetAllocator());
        Value data(value.c_str(), value.size(), m_doc.GetAllocator());
        m_value.AddMember(key, data, m_doc.GetAllocator());
        return JValue(m_value[name.c_str()], m_doc);
    }

    JValue add(const char* name, std::string& value)
    {
        Value key(name, m_doc.GetAllocator());
        Value data(value.c_str(), value.size(), m_doc.GetAllocator());
        m_value.AddMember(key, data, m_doc.GetAllocator());
        return JValue(m_value[key], m_doc);
    }

    JValue add(const char* name, Value& v)
    {
        m_value.AddMember(StringRef(name), v, m_doc.GetAllocator());
        return JValue(m_value[name], m_doc);
    }

    JValue add(const char* name)
    {
        Value v(kObjectType);
        return add(name, v);
    }

    JValue add(const char* name, Type t)
    {
        Value v(t);
        return add(name, v);
    }


    Value& m_value;
    Document& m_doc;
};




#endif //RAPIDJSONBUILDER_H_

