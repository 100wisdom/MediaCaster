/*    file: TcpJsonAVFormat.h
 *    desc:
 *   
 * created: 2015-11-22
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined TcpJsonAVFormat_H_
#define TcpJsonAVFormat_H_

#include "BasicType.h"
#include "Socket.h"
#include "MediaTransport.h"
#include "TByteBuffer.h"


struct AVIOContext;

class TcpJsonAVFormat
{
public:
    TcpJsonAVFormat();
    ~TcpJsonAVFormat();

    int open(const char* ip, int port);
    void close();
    bool isOpen();

    int read(uint8_t *buf, int buf_size);
    int64_t seek(int64_t offset, int whence);

    AVIOContext* create();

    int play(const std::string& name);

    int64_t  getTotalBytes();

    bool readFrame();

public:
    static int ps_read_packet(void *opaque, uint8_t *buf, int buf_size);
    static int64_t ps_seek(void *opaque, int64_t offset, int whence);

private:
    comn::Socket m_socket;
    int64_t m_totalBytes;

    av::MediaTransportHeader m_header;
    bool m_headerReady;
    int m_recvSize;
    comn::ByteBuffer    m_buffer;
};




#endif //TcpJsonAVFormat_H_

