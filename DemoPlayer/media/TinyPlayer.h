/*    file: TinyPlayer.h
 *    desc:
 *   
 * created: 2015-11-22
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined TINYPLAYER_H_
#define TINYPLAYER_H_


#include "MediaType.h"
#include <string.h>

class TinyPlayer
{
public:

    virtual ~TinyPlayer() {}

    virtual int setParam(const char* key, const char* value) =0;

    virtual int open(const char* url) =0;

    virtual void close() =0;

    virtual bool isOpen() =0;

    virtual void setVideoWnd(HWND hwnd) =0;

    virtual int seek(double position) =0;

};


class TinyPlayerFactory
{
public:
    static void startup();
    static void cleanup();

    static TinyPlayer* create(const char* url, HWND hwnd);
    static void destroy(TinyPlayer* player);

};



#endif //TINYPLAYER_H_

