﻿/*    file: EofDetector.h
 *    desc:
 *   
 * created: 2016-03-07
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined EOFDETECTOR_H_
#define EOFDETECTOR_H_

#include "Ffmpeg.h"

class EofDetector
{
public:
    enum Constant
    {
        ELAPSE_THRESHOLD = 10 * 1000,  /// read 耗时阈值, 单位为微秒
        COUNT_THRESHOLD = 10/// 连续多次 耗时 < 阈值
    };

public:
    EofDetector();
    ~EofDetector();

    bool isEof(int64_t elapse, AVFormatContext* fmt);

    void reset();

protected:
    int m_count;    

};


#endif //EOFDETECTOR_H_

