/*
 * MediaType.h
 *
 *  Created on: 2015年6月1日
 *      Author: chuanjiang.zh@qq.com
 */

#ifndef MEDIATYPE_H_
#define MEDIATYPE_H_

#include "BasicType.h"

enum MediaType
{
    MEDIA_TYPE_NONE  = -1,
    MEDIA_TYPE_VIDEO = 0,
    MEDIA_TYPE_AUDIO
};

enum MediaCodec
{
    MEDIA_CODEC_NONE = 0,

    MEDIA_CODEC_H264 = 28,
    MEDIA_CODEC_VP8  = 141,
    MEDIA_CODEC_WMV  = 72,
    MEDIA_CODEC_MPEG4= 13,
    MEDIA_CODEC_THEORA=31,

    MEDIA_CODEC_G711U = 65542,
    MEDIA_CODEC_G711A,

    MEDIA_CODEC_MP3 = 0x15001,
    MEDIA_CODEC_AAC = 0x15002,
    MEDIA_CODEC_AC3 = 0x15003,
    MEDIA_CODEC_VORBIS = 0x15005,
    MEDIA_CODEC_WMA = 86054,

};

enum MediaFlag
{
    MEDIA_FLAG_KEY = 0x01
};

struct MediaPacket
{
    int type;

    uint8_t*    data;
    size_t      size;

    int     flags;

    int64_t ts;
    int32_t duration;

};




#endif /* MEDIATYPE_H_ */
