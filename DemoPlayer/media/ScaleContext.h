/*    file: ScaleContext.h
 *    desc:
 *   
 * created: 2014-01-21 16:58:16
 *  author: zhengchuanjiang
 * company: 
 */ 


#if !defined SCALECONTEXT_H_
#define SCALECONTEXT_H_


#include "Ffmpeg.h"
////////////////////////////////////////////////////////////////////////////
class ScaleContext
{
public:
    explicit ScaleContext(PixelFormat destFmt = PIX_FMT_RGB32);
    ~ScaleContext();

    void reset();

    bool update(AVFrame* pFrame, int width, int height);

    bool update(AVFrame* pFrame, int width, int height, PixelFormat destFmt);

public:
    SwsContext* m_pContext;
    
    PixelFormat m_fmt;

    int m_srcWidth;
    int m_srcHeight;

    int m_outWidth;
    int m_outHeight;

protected:
    PixelFormat m_destFormat;

};

////////////////////////////////////////////////////////////////////////////
#endif //SCALECONTEXT_H_

