/*    file: TinyPlayer.cpp
 *    desc:
 * 
 * created: 2015-11-22
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include "TinyPlayer.h"
#include "CTinyPlayer.h"
#include "CLog.h"
#include "PSPlayer.h"
#include "TStringUtil.h"


extern "C"
{
    #include <libavfilter/avfilter.h>

};

void TinyPlayerFactory::startup()
{
    av_register_all();
    avformat_network_init();

    //avfilter_register_all();
    CLog::setLogger(CLog::DEBUGWINDOW, CLog::kNone);
}

void TinyPlayerFactory::cleanup()
{

}

TinyPlayer* TinyPlayerFactory::create(const char* url, HWND hwnd)
{
    TinyPlayer* player = NULL;
    if (comn::StringUtil::startsWith(url, "ps://"))
    {
        player = new CTinyPlayer();
        //player = new PSPlayer();
    }
    else
    {
        player = new CTinyPlayer();
    }
    player->setVideoWnd(hwnd);
    return player;
}

void TinyPlayerFactory::destroy(TinyPlayer* player)
{
    if (player)
    {
        player->close();
    }

    delete player;
}



