/*
 * RtpPackager.h
 *
 *  Created on: 2015年7月5日
 *      Author: terry
 */

#ifndef RTPPACKAGER_H_
#define RTPPACKAGER_H_

#include "MediaPacket.h"
#include "MediaType.h"

namespace av
{

struct RtpPacket
{
    uint8_t*    data;
    int         size;
    uint32_t    ts;
    bool        mark;
    uint8_t		pt;		/// payload type

};

/**
 * 打包器接收接口
 */
class RtpPackagerSink
{
public:
    virtual ~RtpPackagerSink() {}

    /**
     * 接收拆包
     * @param pkt
     */
    virtual void onSlicePacket(const RtpPacket& pkt) =0;
};


/**
 * RTP打包器
 */
class RtpPackager
{
public:
	virtual ~RtpPackager() {}

	/**
	 * 将媒体包拆包为rtp包
	 * @param pkt
	 * @param maxSize
	 * @param pSink
	 */
    virtual void slice(const MediaPacket& pkt, int maxSize, RtpPackagerSink* pSink) =0;

    /**
     * 从RTP包组合为媒体包
     * @param pktIn
     * @param pktOut
     * @return true 表示有媒体包组合成功
     */
    virtual bool join(const RtpPacket& pktIn, MediaPacket& pktOut) =0;

    /**
     * 重置
     */
    virtual void reset() =0;

};




} /* namespace av */

#endif /* RTPPACKAGER_H_ */
