﻿/*    file: EofDetector.cpp
 *    desc:
 * 
 * created: 2016-03-07
 *  author: chuanjiang.zh@qq.com
 * company: 
 */

#include "EofDetector.h"


EofDetector::EofDetector():
    m_count()
{

}

EofDetector::~EofDetector()
{

}

bool EofDetector::isEof(int64_t elapse, AVFormatContext* fmt)
{
    if (!fmt)
    {
        return true;
    }
    
    if (fmt->pb && fmt->pb->eof_reached == 0)
    {
        return false;
    }

    if (elapse > ELAPSE_THRESHOLD)
    {
    	m_count = 0;
    }
    else
    {
        m_count ++;
    }

    return (m_count >= COUNT_THRESHOLD);
}

void EofDetector::reset()
{
    m_count = 0;
}

