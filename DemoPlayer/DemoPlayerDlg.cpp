
// DemoPlayerDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "DemoPlayer.h"
#include "DemoPlayerDlg.h"

#include "Socket.h"
#include "Base64.h"

#include "Ffmpeg.h"
#include "TFileUtil.h"
#include "IniFile.h"
#include "Path.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CDemoPlayerDlg 对话框




CDemoPlayerDlg::CDemoPlayerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDemoPlayerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CDemoPlayerDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    
    DDX_Control(pDX, IDC_SLIDER_POS, m_slider);
}

BEGIN_MESSAGE_MAP(CDemoPlayerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
    ON_BN_CLICKED(IDC_BUTTON_PLAY, &CDemoPlayerDlg::OnBnClickedButtonPlay)
    ON_BN_CLICKED(IDC_BUTTON_STOP, &CDemoPlayerDlg::OnBnClickedButtonStop)
    ON_WM_DESTROY()
    ON_BN_CLICKED(IDC_BUTTON_TEST, &CDemoPlayerDlg::OnBnClickedButtonTest)
    ON_BN_CLICKED(IDC_BUTTON_ADD_MUCH, &CDemoPlayerDlg::OnBnClickedButtonAddMuch)
    ON_BN_CLICKED(IDC_BUTTON_ADD2, &CDemoPlayerDlg::OnBnClickedButtonAdd2)
    ON_WM_TIMER()
    ON_WM_HSCROLL()
END_MESSAGE_MAP()


// CDemoPlayerDlg 消息处理程序

BOOL CDemoPlayerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	init();

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CDemoPlayerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CDemoPlayerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CDemoPlayerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CDemoPlayerDlg::OnDestroy()
{
    CDialog::OnDestroy();

    uninit();
}


void CDemoPlayerDlg::OnBnClickedButtonPlay()
{
    CString url;
    GetDlgItemText(IDC_EDIT_URL, url);

    CString srcUrl;
    GetDlgItemText(IDC_EDIT_SRC, srcUrl);

    bool useMyPlayer = (IsDlgButtonChecked(IDC_CHECK_PLAYER) == BST_CHECKED);

    CString scale;
    GetDlgItemText(IDC_COMBO_SCALE, scale);
    CString tmStart;
    GetDlgItemText(IDC_EDIT_START, tmStart);

    //m_player->setParam("scale", scale.GetString());

    if (useMyPlayer)
    {
        m_player->close();
        m_player->open(url);
    }
    else
    {
        std::string cmdline = comn::StringUtil::format(
            getPlayerCmd(),
            url.GetString());
        m_ffplay.close();
        m_ffplay.open(cmdline.c_str());
    }

}

void CDemoPlayerDlg::OnBnClickedButtonStop()
{
    m_player->close();

    m_ffplay.close();

    CWnd* videoWnd = GetDlgItem(IDC_STATIC_VIDEO);
    if (videoWnd)
    {
        videoWnd->InvalidateRect(NULL,TRUE);
    }

    /// 停止定时添加
    KillTimer(100);
}


void av_log_callback(void *avcl, int level, const char *fmt,
                     va_list vl)
{
    av_log_default_callback(avcl, level, fmt, vl);

    if (level <= AV_LOG_WARNING)
    {
        const size_t MAX_SIZE = 1024 * 2;
        char buffer[MAX_SIZE] = {0};

        char* buf = buffer;
        int len = comn::StringUtil::vsprint(&buf, MAX_SIZE, fmt, vl);

        //
        OutputDebugString(buf);
        comn::FileUtil::write(buf, len, "ffmpeg_player.log", true);

        if (buf != buffer)
        {
            free(buf);
        }
    }
}

void CDemoPlayerDlg::init()
{
    av_register_all();
    av_log_set_level(AV_LOG_TRACE);
    av_log_set_callback(av_log_callback);

    TinyPlayerFactory::startup();

    
    HWND hwnd = 0;
    CWnd* videoWnd = GetDlgItem(IDC_STATIC_VIDEO);
    if (videoWnd)
    {
        hwnd = videoWnd->m_hWnd;
    }
    m_player.reset(TinyPlayerFactory::create("ps://", hwnd), TinyPlayerFactory::destroy);

    m_sourceIdx = 0;

    const char* host = comn::SockAddr::getHostIP();
    SetDlgItemText(IDC_EDIT_IP, host);
    SetDlgItemInt(IDC_EDIT_PORT, 1564);
    SetDlgItemInt(IDC_EDIT_RTSP_PORT, 1554);

    SetDlgItemText(IDC_EDIT_IP, "192.168.3.14");

    SetDlgItemText(IDC_EDIT_CAMID, "20");

    m_slider.SetRange(0, 10000);
    m_slider.SetLineSize(10);
    m_slider.SetPageSize(20);

    CString url = "rtsp://192.168.2.11/e:/media/record.ts";
    //url = "h:\\data\\kid.ts";
    //url = "http://192.168.2.13:9000/media/record.mp4";
    //url = "D:\\Demo\\DemoFfmpeg\\vs2008\\demoOut\\mpeg.mpg";
    //url = "h:\\data\\1080.mp4";
    url = "rtsp://admin:sjld16301@192.168.3.65";
    //url = "h:\\demo\\full.ps";
    //url = "http://192.168.3.113:8080/data/265.mp4";
    //url = "http://192.168.3.11:9000/media/record.ps";
    //url = "rtsp://192.168.3.11/e:/media/me.mkv";
    //url = "rtsp://192.168.3.113/me.mkv";

    SetDlgItemText(IDC_EDIT_SRC, url);
    SetDlgItemInt(IDC_EDIT_START, 0);

    SetDlgItemText(IDC_EDIT_PLAYER, "E:\\tools\\VideoLAN\\VLC\\vlc.exe %s");
}

void CDemoPlayerDlg::uninit()
{
    m_player.reset();
}


CString CDemoPlayerDlg::getPlayerCmd()
{
    CString cmd;
    GetDlgItemText(IDC_EDIT_PLAYER, cmd);
    return cmd;
}

void CDemoPlayerDlg::play(CString url)
{
    if (m_player->isOpen())
    {
        m_player->close();
    }

    m_player->open(url);
}

void CDemoPlayerDlg::stop()
{
    m_player->close();
}

void CDemoPlayerDlg::OnBnClickedButtonTest()
{
    CString srcUrl;
    GetDlgItemText(IDC_EDIT_SRC, srcUrl);

    play(srcUrl);
}



bool loadPlaySource(PlaySourceArray& sources, const char* filename)
{
    comn::IniFile inifile;
    if (!inifile.load(filename))
    {
        return false;
    }

    size_t count = inifile.getSectionCount();
    for (size_t i = 0; i < count; i ++)
    {
        comn::IniFile::IniSection& section = inifile.getSection(i);
        if (!section.exist("Url"))
        {
            continue;
        }

        PlaySource source;
        section.get("Url", source.url);

        std::string mode;
        section.get("Mode", mode);

        sources.push_back(source);
    }

    return true;
}

void CDemoPlayerDlg::OnBnClickedButtonAddMuch()
{
    if (!loadSourceFromFile())
    {
        AfxMessageBox("没有找到文件 trans_task.ini");
        return;
    }

    PlaySourceArray& sources = m_sources;
    for (size_t i = 0; i < sources.size(); i ++)
    {
        PlaySource& source = sources[i];
        startSource(source);
    }
}

bool CDemoPlayerDlg::loadSourceFromFile()
{
    std::string workDir = comn::Path::getWorkDir();
    std::string filePath = comn::Path::join(workDir, "trans_task.ini");

    PlaySourceArray& sources = m_sources;
    sources.clear();

    return loadPlaySource(sources, filePath.c_str());
}

void CDemoPlayerDlg::OnBnClickedButtonAdd2()
{
    loadSourceFromFile();

    if (m_sources.size() > 0)
    {
        SetTimer(100, 1000 * 5, NULL);
    }
    
}

void CDemoPlayerDlg::OnTimer(UINT_PTR nIDEvent)
{
    if (nIDEvent == 100)
    {
    }

    CDialog::OnTimer(nIDEvent);
}


void CDemoPlayerDlg::testJson()
{
}

void CDemoPlayerDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
    if (nSBCode == TB_ENDTRACK)
    {
        int pos = m_slider.GetPos();
        double scalePos = (double)pos / 10000;

        TRACE("slider: %d, scale:%f\n", pos, scalePos);

        m_player->seek(scalePos);
    }
    
    CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}


void CDemoPlayerDlg::startSource(const PlaySource& source)
{
    CString ip;
    GetDlgItemText(IDC_EDIT_IP, ip);
    int port = GetDlgItemInt(IDC_EDIT_PORT, NULL, FALSE);

}
