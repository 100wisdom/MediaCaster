/*    file: Process.h
 *    desc:
 *   
 * created: 2015-08-10
 *  author: chuanjiang.zh@qq.com
 * company: 
 */ 


#if !defined PROCESS_H_
#define PROCESS_H_

#include "BasicType.h"
////////////////////////////////////////////////////////////////////////////
class Process
{
public:
    Process():
      m_handle()
    {

    }

    ~Process()
    {
    }

    bool open(const char* cmdline)
    {
        STARTUPINFO si;
        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);
        //si.dwFlags |= STARTF_USESTDHANDLES;
        //si.hStdOutput = 0;

        PROCESS_INFORMATION pinfo;
        memset(&pinfo, 0, sizeof(pinfo));

        BOOL ret = CreateProcessA(NULL, (LPSTR)cmdline,
            NULL, NULL,
            FALSE, 0, NULL, NULL, &si,
            &pinfo);

        m_handle = pinfo.hProcess;

        return (ret != FALSE);
    }

    void close()
    {
        if (m_handle != NULL)
        {
            TerminateProcess(m_handle, -1);
            CloseHandle(m_handle);
            m_handle = 0;
        }
    }

    bool isOpen()
    {
        return (m_handle != NULL);
    }

protected:
    HANDLE  m_handle;

};
////////////////////////////////////////////////////////////////////////////
#endif //PROCESS_H_

